#!/bin/sh

set -e

dradis

if ! systemctl is-active -q dradis; then
    echo "The service fails to start"
    exit 1
fi

# test code response of 127.0.0.1:3000
http_code=$(curl -o /dev/null -s -w "%{http_code}\n" http://127.0.0.1:3000)
if [ $http_code -ne 200 ]; then
    echo "There is a problem with the server. Code response is" $http_code
    exit 1
else
    echo "The server is up"
fi

if ! curl -s http://127.0.0.1:3000 | grep -q "shared password"; then
    echo "Text \"shared password\" is not found"
    exit 1
else
    echo "Page is correctly displayed"
fi

dradis-stop | true

if ! [ $(systemctl is-active dradis) = inactive ]; then
    echo "The service fails to stop"
    exit 1
fi
